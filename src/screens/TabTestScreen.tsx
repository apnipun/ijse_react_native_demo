import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { TabScreen1 } from "./TabScreen1.tsx";
import { TabScreen2 } from "./TabScreen2.tsx";
import { TabScreen3 } from "./TabScreen3.tsx";
import { TabScreen4 } from "./TabScreen4.tsx";
import { Text } from "react-native";

const Tab = createBottomTabNavigator();
export const TabTestScreen = (props: any) => {

  return (
    <Tab.Navigator initialRouteName={'TabScreen1'}
                   screenOptions={{headerShown: false}}>
      <Tab.Screen name={'TabScreen1'} component={TabScreen1}
                  options={{
                    title: 'Screen 1',
                    tabBarIcon: (iconProps) => {
                      return <Text style={{fontSize: 25, fontWeight: 'bold'}}>{iconProps.focused ? 'A' : 'B'}</Text>;
                    },
                  }}/>
      <Tab.Screen name={'TabScreen2'} component={TabScreen2}
                  options={{
                    title: 'Screen 2',
                  }}/>
      <Tab.Screen name={'TabScreen3'} component={TabScreen3} />
      <Tab.Screen name={'TabScreen4'} component={TabScreen4} />
    </Tab.Navigator>
  );

};
