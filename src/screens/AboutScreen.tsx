import { ScrollView, View } from "react-native";
import { CustomButton } from "../components/CustomButton.tsx";
import React from "react";
import { CustomTitle } from "../components/CustomTitle.tsx";
import Animated, { useSharedValue, withSpring } from "react-native-reanimated";

export const AboutScreen = (props: any) => {

  const sampleViewHeight = useSharedValue(0);

  return (
    <ScrollView>
      <CustomTitle
        title={'This is the About Page'}
        subTitle={'Click the below button to Go Back'}/>

      <CustomButton
        label={'Open / Close Animated View'}
        onPress={() => {
          sampleViewHeight.value = sampleViewHeight.value > 0 ? withSpring(0) : withSpring(200);
        }}
      />

      <Animated.View style={{
        backgroundColor: 'red',
        height: sampleViewHeight}}>
      </Animated.View>

      <CustomButton
        label={'Go Back'}
        onPress={() => {
          props.navigation.goBack();
        }}
      />
    </ScrollView>
  );

};
