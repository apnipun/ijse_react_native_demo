import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { DrawerScreen1 } from "./DrawerScreen1.tsx";
import { DrawerScreen2 } from "./DrawerScreen2.tsx";
import { TabTestScreen } from "./TabTestScreen.tsx";

const Drawer = createDrawerNavigator();

export const DrawerTestScreen = (props: any) => {

  return (
    <Drawer.Navigator initialRouteName={'DrawerScreen1'}
                      screenOptions={{headerShown: false, drawerPosition: 'right'}}>
      <Drawer.Screen name={'DrawerScreen1'} component={DrawerScreen1} />
      <Drawer.Screen name={'DrawerScreen2'} component={DrawerScreen2} />
      <Drawer.Screen name={'TabTestScreen'} component={TabTestScreen} />
    </Drawer.Navigator>
  );

};
