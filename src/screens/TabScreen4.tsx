import { ScrollView } from "react-native";
import { CustomButton } from "../components/CustomButton.tsx";
import React from "react";
import { CustomTitle } from "../components/CustomTitle.tsx";
import { DrawerActions } from "@react-navigation/native";

export const TabScreen4 = (props: any) => {

  return (
    <ScrollView>
      <CustomTitle
        title={'This is the Tab Screen 4'}
        subTitle={'Click the below button to Go Back'}/>

      <CustomButton
        label={'Open Drawer'}
        onPress={() => {
          props.navigation.dispatch(DrawerActions.openDrawer());
        }}
      />

      <CustomButton
        label={'Go Back'}
        onPress={() => {
          props.navigation.goBack();
        }}
      />
    </ScrollView>
  );

};
